/// <reference types="cypress" />

// Load Chance
var Chance = require('chance');

// Instantiate Chance so it can be used
var chance = new Chance();

/* Mocha -> Test Runner  = describe/ context/ it*/

describe('Cadastro', () => {
    it('Quando eu informar os dados e finalizar, então cadastro deve ser efetuado', () => {
        cy.visit('https://form-agilizei.netlify.app/index.html');

        // Inputs de texto / textarea/ email -> type
        cy.get('input[name="firstName"]').type(chance.first());
        cy.get('input[name="lastName"]').type(chance.last());
        cy.get('textarea[name="adress"]').type(chance.address());
        cy.get('input[name="emailAdress"]').type(chance.email());

        // Inputs radio / checkboxes -> check
        cy.get('input[value="m"]').check();
        cy.get('input[type="checkbox"]').check('Netflix');
        cy.get('input[type="checkbox"]').check('Dormir');

        // Inputs do tipo combobox / select -> select
        cy.get('select#countries').select('Dinamarca', { force: true });
        cy.get('select#years').select('1989', { force: true });
        cy.get('select#months').select('Setembro', { force: true });
        cy.get('select#days').select('12', { force: true });

        cy.get('input#firstpassword').type('@Pcs123');
        cy.get('input#secondpassword').type('@Pcs123');

        /* FINALIZAR CADASTRO */
        cy.get('button#submitbtn').click();

        cy.url().should('contains', 'listagem');

        cy.contains('Listagem').should('be.visible').then(() => {
            const teste = cy.get('tbody > tr > td').eq(0).invoke('toString');
            cy.log(teste)
        })
    });
});