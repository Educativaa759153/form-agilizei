/// <reference types="cypress" />


/* Controle de estado da aplicação */

// Requisições HTTP
// Através dos storages
// -> Local storages

/* Controle de estado da aplicação */
// -> como o sistema se comporta com requisições
// -> Requisição HTTP - API
// -> Local storages
// -> Session Storage

/* Ao clicar Finalizar Cadastro */

/* 
1- Pega todas as informações do form
2- Armazena isso em um storage
3- LocalStorage tem padrão - Chave = valor
4- Atributo key data do Storage
*/

describe('Listagem', () => {
    it('Quando não houver cadastro, então a listagem deve estar vazia', () => {
        
        cy.fixture("listagem-vazia").then(data => {

            window.localStorage.setItem('data', JSON.stringify(data))
            
        });        

        cy.visit('https://form-agilizei.netlify.app/listagem.html');

        /* A listagem deve esta vazia */
        cy.get('table tbody tr').should('have.length', 0);

    });

    it('Quando houver um ou mais cadastros, deve exibir cada registros', () => {
        cy.fixture("listagem-com-itens").then(data => {

            window.localStorage.setItem('data', JSON.stringify(data))

        });        

        cy.visit('https://form-agilizei.netlify.app/listagem.html');

        /* A listagem deve conter 2 cadastros */
        cy.get('table tbody tr').should('have.length', 2);
    });
});