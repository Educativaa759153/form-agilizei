# README #

This README would normally document whatever steps are necessary to get your application up and running.

### INSTALAR

npm init --yes
npm i cypress@8.5.0 -D
npm i change -D
npm i cypress-multi-reporters
npm i mochawesome
npm i mochawesome-merge
npm i mochawesome-report-generator
npm i -D rimraf

### Pastas Cypress
fixtures -> armanezar dados de mocks / dados usados no teste
integration -> armazenar os arquivos de testes
plugins -> configuração de plugins
support -> comandos customizados / arquivos de suporte ao teste

screenshots -> gerada sempre que voce tira uma screenchots no teste
videos -> gerada quando você faz gravação de teste

cypress.json -> arquivos de configurações do cypress
url base
tamanho de tela
variaveis de ambiente
video
modifiacar pastas padrao
-> Adicionar "$schema": "https://on.cypress.io/cypress.schema.json"

Usando Change/ Faker
-> https://chancejs.com/usage/node.html

// Load Chance
var Chance = require('chance');

// Instantiate Chance so it can be used
var chance = new Chance();

Usando Chai Assertion
-> https://www.chaijs.com/api/bdd/

### JS Testing Best Practices

1 Adicionar 3 partes no nome de cada teste
1.1 Oque esta sendo testado
1.2 Quais as condições / circunstâncias
1.3 Qual o resultado esperado

2 Cadastro com sucesso ->
2.1 Quando eu informar os dados e finalizar, então o cadastro deve ser efetuado.

Analise de médio/ longo prazo
    -> Quais os teste mais falham?
    -> Histórico de quantidaes passado vs falhado
    -> Histórico de execuções
    -> Duração das suites de testes
Cypress Dashboard - Armaneza informações dos testes

Mochawesome -> biblioteca de geração de relatórios de testes

Cypress-multi-reporters - relatórios diferentes em um mesmo projeto
    -> XML - CI
    -> Json - HTML

Mochawesome -> Gerar um arquivo por SPEC - JSON ou HTML
    -> Geralmente queremos o relatório em um arquivo unicoe para isso usamos o Mochawesome merge

Mochawesome-merge -> Ele mescla os resultados de varios arquivos JSON em um só.
Mochawesome-report-generator -> Gera relatório em HTML baseado no arquivo mesclado

Rimraf -> Executa comandos de exclusão